console.log("hello from JS"); 

 
let params = new URLSearchParams(window.location.search)

let id = params.get('kahitAno')
console.log(id)

//lets capture the sections of the html body
let name = document.querySelector("#courseName")
let desc = document.querySelector("#courseDesc")
let price = document.querySelector("#coursePrice")

fetch(`https://mighty-refuge-32843.herokuapp.com/api/courses/${id}`).then(res => res.json()).then(data => {
    console.log(data)

    name.innerHTML = data.name
    desc.innerHTML = data.description
    price.innerHTML = data.price    
}) //if the result is displayed in the console. it means you were able to properly pass the courseid and successfully created your fetch request. 
