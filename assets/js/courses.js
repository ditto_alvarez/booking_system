console.log("hello from JS")
let modalButton = document.querySelector('#adminButton')
//CAPTURE the html body which will display the content coming from the db.
let container = document.querySelector('#coursesContainer')
let cardFooter; 
//we are going to take the value of the isAdmin property from the local storage.
let isAdmin = localStorage.getItem("isAdmin")


if(isAdmin == "false" || !isAdmin){
   //if a user is a regular user, do not show the addcourse button.
   modalButton.innerHTML = null; 
}else{
    modalButton.innerHTML = `
      <div class="col-md-2 offset-md-10"><a href="./addCourse.html" class="btn btn-block btn-primary">Add Course</a>
      </div>
    `
}

fetch('https://mighty-refuge-32843.herokuapp.com/api/courses/').then(res => res.json()).then(data => {
	console.log(data); 
	//declare a variable that will display a result in the browser depending on the return
	let courseData; 
	//create a control structure that will determine the value that the variable will hold. 
	if(data.length < 1){
        courseData = "No Course Available"
	}else{
       //we will iterate the courses collection and display each course inside the browser
       courseData = data.map(course => {
       	//lets check the make up of each element inside the courses collection
       	console.log(course._id); 

        //if the user is a regular user, display the enroll button and display course button.
        if(isAdmin == "false" || !isAdmin)
        {
           cardFooter = ` <a href="./course.html?kahitAno=${course._id}" class="btn btn-primary text-white btn-block">View Course details</a>`
        }else{
           cardFooter = `
              <a href="./editCourse.html?kahitAno=${course._id}" class="btn btn-primary text-white btn-block"> Edit </a>
              <a href="./deleteCourse.html?kahitAno=${course._id}" class="btn btn-primary text-white btn-block"> Disable Course </a>
           `
        }
       	return (
       		`
       		<div class="col-md-6 my-3">
			   	<div class="card">
			   		<div class="card-body">
			   			<h5 class="card-title"> ${course.name}</h5>
			   			<p class="card-text text-left">
                          ${course.description}
			   			</p>
			   			<p class="card-text text-left">
                          ${course.price}
			   			</p>
			   			<p class="card-text text-left">
                          ${course.createdOn}
			   			</p>
			   		</div>
			   		<div class="card-footer">
			   		       ${cardFooter}
			   		</div>
			   	</div>
			 </div>
       		` //we attached a query string in the URL which allows us to embed the ID from the database record into the query strig. 
       		// ? -> inside the URL "acts" as a "separator", it indicates the end of a URL resouce path, and indicates the start of the *query string*. 
       		// # -> this was originally "used" to jump to an specific element with same id name/value. 
       		)
       }).join("")//we used the join() to create a return of a new string
       //it "contatenated" all the objects inside the array and converted each to a string data type. 
	}
	container.innerHTML = courseData; 
})
